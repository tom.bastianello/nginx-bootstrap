## Image
registry.gitlab.com/tom.bastianello/nginx-bootstrap

## Dockerfile
```
FROM nginx:alpine
RUN apk add curl --no-cache

ADD startup /startup
RUN chmod +x /startup

CMD ["sh", "-c"]
ENTRYPOINT "/startup"
```

## Bootstrap script
```
echo "Pulling $CONFIG_URL"
curl $CONFIG_URL -o /etc/nginx/nginx.conf
nginx
```

## Env Variables
- CONFIG_URL

## Usage
`docker run -e CONFIG_URL="<your-image-url>" registry.gitlab.com/tom.bastianello/nginx-bootstrap`